#ifndef __WRAPPER_H
#define __WRAPPER_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct Money Money;
	Money* makeMoney();
	void set_var(Money* m, int var);
	int get_var(Money* m);
	void burnMoney(Money* m);

	typedef struct Girl Girl;
	Girl* makeGirl();
    void love(Money* g, const char* name);
	void destroyGirl(Money* g);

#ifdef __cplusplus
}
#endif
#endif
