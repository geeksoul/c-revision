#ifndef __TEST2_H
#define __TEST2_H
#include "test.h"
#include <iostream>
using namespace std;
class Girl : public Money {
	private:
		int age = 20;
	public:
		virtual void love(string name);
		virtual ~Girl() { cout << "Girl destructor" << endl; }
};
#endif
