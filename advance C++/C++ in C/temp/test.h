#ifndef __TEST_H
#define __TEST_H
#include <iostream>
#include <string>
using namespace std;

class Money {
	private:
		int var = 5;
	public:
		void set_var(int var);
		int get_var();
		virtual void love(string name); 
		virtual ~Money() { cout << "destructor of Money" << endl; }
};
#endif
