#include "wrapper.h"
#include "test.h"
#include "test2.h"
#include <string>
using namespace std;

extern "C" {
	Money* makeMoney() {
		return new Money();
	}
	void set_var(Money* m, int var) {
		m->set_var(var);
	}
	int get_var(Money* m) {
		return m->get_var();
	}
	void burnMoney(Money* m) {
		delete m;
	}
	Girl* makeGirl() {
		return new Girl();
	}
	void love(Money* g, const char* name) {
		string name_ = name;
		g->love(name_);
	}
	void destroyGirl(Money *g) {
		delete g;
	}
}
