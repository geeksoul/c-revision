#include "wrapper.h"
#include <stdio.h>

int main(int argc, char** argv) {
	void* m = makeMoney();
	set_var(m, 10);
	printf("variable is: %d\n", get_var(m));
	love(m, "Pooja");
	burnMoney(m);
	void* g = makeGirl();
	love(g, "Pooja");
	destroyGirl(g);
	void* g1 = makeGirl();
	love(g1, "Deepanshu");
	destroyGirl(g1);
	return 0;
}
